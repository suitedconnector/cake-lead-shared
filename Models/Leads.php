<?php
/**
 * Created by PhpStorm.
 * User: tcobb
 * Date: 2018/02/06
 * Time: 16:04
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
	protected $table = 'leads';
	public $timestamps = TRUE;
	
	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at'];
	
	protected $dates = [
		'created_at',
		'updated_at',
		'posted_at',
		'server_pixel_fired_at',
		'html_pixel_fired_at'
	];
	
	/* automatically deserialize these json strings */
	protected $casts = [
		'posted_data' => 'array',
		'secure' => 'boolean',
		'primary_vertical' => 'boolean',
		'payable' => 'boolean',
		'throttled' => 'boolean',
		'success' => 'boolean'
	];
}
