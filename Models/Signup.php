<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Signup extends Model {
	use ColumnEnumerationModelTrait;
	protected $table = 'signups';
	
	public $timestamps = FALSE;
	
	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_added', 'date_updated'];
	
	protected $dates = [
		'date_added',
		'date_updated'
	];
	
	/* automatically deserialize these json strings */
	protected $casts = [
		'meta_data' => 'array'
	];
	
	/**
	 * @return array
	 */
	public function getAllowedColumns(): array {
		return array_diff($this->getColumns(), $this->guarded);
	}
	
	/**
	 * @return string
	 */
	public function getFullNameAttribute() {
		return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
	}
	
	/**
	 * @return string
	 */
	public function getEmailDomainAttribute() {
		if (filter_var($this->attributes['email'], FILTER_VALIDATE_EMAIL)) {
			return substr(strrchr($this->attributes['email'], "@"), 1);
		} else {
			return '';
		}
	}
	
	/**
	 * @return mixed
	 */
	public function getHomePhoneAttribute() {
		return (isset($this->attributes['phone_primary']) ? $this->attributes['phone_primary'] :
			(isset($this->attributes['phone_home']) ? $this->attributes['phone_home'] : NULL));
	}
	
	/**
	 * @param $value
	 */
	public function setUuidAttribute($value) {
		// if null, or not a proper uuid sent, then generate one
		if (!isset($value) || !is_string($value) || (is_string($value) && strlen(trim($value)) !== 36)) {
			$this->generateUuid();
		} else {
			$this->attributes['uuid'] = $value;
		}
	}
	
	/**
	 *
	 */
	public function generateUuid() {
		if (!isset($this->attributes['uuid'])) {
			$this->attributes['uuid'] = (string)Uuid::generate(4);
		}
	}
	
	/**
	 * @param $value
	 */
	public function setFirstNameAttribute($value) {
		$this->attributes['first_name'] = ucwords($value);
	}
	
	/**
	 * @param $value
	 */
	public function setLastNameAttribute($value) {
		$this->attributes['last_name'] = ucwords($value);
	}
	
	/**
	 * @param $value
	 */
	public function setEmailAttribute($value) {
		$this->attributes['email'] = strtolower($value);
	}
	
	/**
	 * @param $value
	 */
	public function setAddressAttribute($value) {
		$this->attributes['address'] = ucwords($value);
	}
	
	/**
	 * @param $value
	 */
	public function setCityAttribute($value) {
		$this->attributes['city'] = ucwords($value);
	}
	
	/**
	 * @param $value
	 */
	public function setStateAttribute($value) {
		$this->attributes['state'] = strtoupper($value);
	}
	
	/**
	 * @param $value
	 */
	public function setPropertyCityAttribute($value) {
		$this->attributes['property_city'] = ucwords($value);
	}
	
	/**
	 * @param $value
	 */
	public function setPropertyStateAttribute($value) {
		$this->attributes['property_state'] = strtoupper($value);
	}
	
	/**
	 * @param $value
	 */
	public function setPhonePrimaryAttribute($value) {
		$this->attributes['phone_primary'] = preg_replace('/\D+/', '', (string)$value);
	}
	
	/**
	 * @param $value
	 */
	public function setPhoneWorkAttribute($value) {
		$this->attributes['phone_work'] = preg_replace('/\D+/', '', (string)$value);
	}
	
	/**
	 * @param $value
	 */
	public function setPhoneMobileAttribute($value) {
		$this->attributes['phone_mobile'] = preg_replace('/\D+/', '', (string)$value);
	}
	
	/**
	 * @param $value
	 */
	public function setPhoneHomeAttribute($value) {
		$this->attributes['phone_home'] = preg_replace('/\D+/', '', (string)$value);
	}
	
	// to cover loanToValue we need to wrap both est_property_value and mortgage_amount
	
	/**
	 * @param $value
	 */
	public function setEstPropertyValueAttribute($value) {
		$this->attributes['est_property_value'] = $value;
		$this->computeLoanToValue();
	}
	
	/**
	 * @param $value
	 */
	public function setMortgageAmountAttribute($value) {
		$this->attributes['mortgage_amount'] = $value;
		$this->computeLoanToValue();
	}
	
	/**
	 *
	 */
	public function computeLoanToValue() {
		if (isset($this->attributes['mortgage_amount']) &&
				(isset($this->attributes['est_property_value']) && ($this->attributes['est_property_value'] > 0))) {
			$this->attributes['loan_to_value'] = $this->attributes['mortgage_amount'] / $this->attributes['est_property_value'];
		} else {
			$this->attributes['loan_to_value'] = NULL;
		}
	}
	
	/**
	 *  Set date added in mountain time
	 *  (called by the observer)
	 */
	public function setDateAddedInDenver() {
		$this->attributes['date_added'] = Carbon::now(new DateTimeZone('America/Denver'));
	}
	
	/**
	 *  set date updated in mountain time
	 *  (called by the observer)
	 */
	public function setDateUpdatedInDenver() {
		$this->attributes['date_updated'] = Carbon::now(new DateTimeZone('America/Denver'));
	}
	
	/* translations */
	/**
	 * @return null|string
	 */
	public function getOptMortgageLateMonthsTextAttribute() {
		if (isset($this->attributes['opt_mortgage_late_months'])) {
			switch ($this->attributes['opt_mortgage_late_months']) {
				case 1:
					$translatedValue = '1 Month Late';
					break;
				case 2:
					$translatedValue = '2 Months Late';
					break;
				case 3:
					$translatedValue = '3 Months Late';
					break;
				case 4:
					$translatedValue = '4 Months Late';
					break;
				case 5:
					$translatedValue = '4+ Months Late';
					break;
				default:
					$translatedValue = 'Yes I am current';
					break;
			}
		} else {
			$translatedValue = NULL;
		}
		
		return $translatedValue;
	}
	
	/**
	 * @return null|string
	 */
	public function getAgentFoundTextAttribute() {
		if (isset($this->attributes['agent_found'])) {
			return $this->attributes['agent_found'] === 1 ? "Yes" : "No";
		}
		return NULL;
	}
	
	/**
	 * @return null|string
	 */
	public function getHomeFoundTextAttribute() {
		if (isset($this->attributes['home_found'])) {
			return $this->attributes['home_found'] === 1 ? "Yes" : "No";
		}
		return NULL;
	}
	
	/**
	 * @return null|string
	 */
	public function getOptForeclosuresTextAttribute() {
		if (isset($this->attributes['opt_foreclosures'])) {
			return $this->attributes['opt_foreclosures'] === 1 ? "Yes" : "No";
		}
		return NULL;
	}
	
	/**
	 * @return null|string
	 */
	public function getOptServedMilitaryTextAttribute() {
		if (isset($this->attributes['opt_served_military'])) {
			return $this->attributes['opt_served_military'] === 1 ? "Yes" : "No";
		}
		return NULL;
	}
	
}
