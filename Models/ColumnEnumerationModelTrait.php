<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

trait ColumnEnumerationModelTrait {

	/**
	 * @return mixed
	 */
	protected function getColumns() {
		return DB::connection($this->connection)
			->getSchemaBuilder()
			->getColumnListing($this->getTable());
	}

}
